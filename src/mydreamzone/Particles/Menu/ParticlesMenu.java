package mydreamzone.Particles.Menu;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.PluginDescriptionFile;

import mydreamzone.Particles.Particle;

public class ParticlesMenu 
{
	public Particle plugin;
	public ParticlesMenu(Particle plugin)
	{
		this.plugin = plugin;
	}
	public void Menu(Player p) 
	{	
		Inventory inv = Bukkit.createInventory(null, 45, "�6Lista dostepnych efektow");
		
	    p.playSound(p.getLocation(), Sound.FIREWORK_BLAST, 100.0F, 100.0F);
	    
	    if(p.hasPermission("MyDreamZoneParticles.Heart"))
	    {
	    	Material material = Material.RED_ROSE;
	    	int amount = 1;
	    	String name = "�aHeart";    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Spell"))
	    {
	    	Material material = Material.POTION;
	    	int amount = 1;
	    	byte data = (byte) 8268;
	    	String name = "�aSpell";    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, data, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Fire"))
	    {
	    	Material material = Material.FIREBALL;
	    	int amount = 1;
	    	String name = "�aFire";    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Cloud"))
	    {
	    	Material material = Material.INK_SACK;
	    	int amount = 1;
	    	byte data = (byte) 15;
	    	String name = "�aCloud";    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, data, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Explode"))
	    {
	    	Material material = Material.TNT;
	    	int amount = 1;
	    	String name = "�aExplode";    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Fireworks"))
	    {
	    	Material material = Material.FIREWORK;
	    	int amount = 1;
	    	String name = "�aFireworks";    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Splash"))
	    {
	    	Material material = Material.WATER_LILY;
	    	int amount = 1;
	    	String name = "�aSplash";    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Wake"))
	    {
	    	Material material = Material.INK_SACK;
	    	int amount = 1;
	    	byte data = (byte) 12; 
	    	String name = "�aWake";    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, data, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Suspended"))
	    {
	    	Material material = Material.COAL;
	    	int amount = 1;
	    	String name = "�aSuspended";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Crit"))
	    {
	    	Material material = Material.NETHER_STAR;
	    	int amount = 1;
	    	String name = "�aCrit";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Magic"))
	    {
	    	Material material = Material.ENDER_PORTAL_FRAME;
	    	int amount = 1;
	    	String name = "�aMagic";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Smoke"))
	    {
	    	Material material = Material.INK_SACK;
	    	int amount = 1;
	    	byte data = (byte) 0;
	    	String name = "�aSmoke";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, data, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Witch"))
	    {
	    	Material material = Material.SKULL_ITEM;
	    	int amount = 1;
	    	byte data = (byte) 1;
	    	String name = "�aWitch";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, data, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Water"))
	    {
	    	Material material = Material.WATER_BUCKET;
	    	int amount = 1;
	    	String name = "�aWater";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Lava"))
	    {
	    	Material material = Material.LAVA_BUCKET;
	    	int amount = 1;
	    	String name = "�aLava";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Angry"))
	    {
	    	Material material = Material.RED_MUSHROOM;
	    	int amount = 1;
	    	String name = "�aAngry";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Note"))
	    {
	    	Material material = Material.NOTE_BLOCK;
	    	int amount = 1;
	    	String name = "�aNote";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Portal"))
	    {
	    	Material material = Material.OBSIDIAN;
	    	int amount = 1;
	    	String name = "�aPortal";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Enchantment"))
	    {
	    	Material material = Material.ENCHANTMENT_TABLE;
	    	int amount = 1;
	    	String name = "�aEnchantment";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Flame"))
	    {
	    	Material material = Material.BLAZE_ROD;
	    	int amount = 1;
	    	String name = "�aFlame";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Lava2"))
	    {
	    	Material material = Material.GLOWSTONE_DUST;
	    	int amount = 1;
	    	String name = "�aLava2";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Redstone"))
	    {
	    	Material material = Material.REDSTONE;
	    	int amount = 1;
	    	String name = "�aRedstone";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Snowball"))
	    {
	    	Material material = Material.SNOW_BALL;
	    	int amount = 1;
	    	String name = "�aSnowball";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Snow"))
	    {
	    	Material material = Material.SNOW_BLOCK;
	    	int amount = 1;
	    	String name = "�aSnow";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Slime"))
	    {
	    	Material material = Material.SLIME_BALL;
	    	int amount = 1;
	    	String name = "�aSlime";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Barrier"))
	    {
	    	Material material = Material.BARRIER;
	    	int amount = 1;
	    	String name = "�aBarrier";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Crack"))
	    {
	    	Material material = Material.CACTUS;
	    	int amount = 1;
	    	String name = "�aCrack";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Appearance"))
	    {
	    	Material material = Material.BONE;
	    	int amount = 1;
	    	String name = "�aAppearance";
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyDreamZoneParticles.Menu"))
	    {
	    	PluginDescriptionFile pdf = plugin.getDescription();
	    	SkullMeta  meta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
	    	meta.setOwner(String.valueOf(pdf.getAuthors()));
	    	ItemStack stack = new ItemStack(Material.SKULL_ITEM,1 , (byte)3);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(ChatColor.GREEN + "Autor: " + String.valueOf(pdf.getAuthors()));
		    lore.add(ChatColor.GREEN + "Version: " + String.valueOf(pdf.getVersion()));
		    meta.setLore(lore);
	    	meta.setDisplayName("�6MyDreamZone Particles");
	    	stack.setItemMeta(meta);	  
		    inv.setItem(43, stack);
	    }
	    p.openInventory(inv);
	}
}
