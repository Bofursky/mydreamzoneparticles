package mydreamzone.Particles.Managers;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemManager
{
	public ItemStack createItem(Material material, Integer amount, String displayName, List<String> lore)
	{
		ItemStack item = new ItemStack(material, amount);
		if(displayName != null || lore != null)
		{
			ItemMeta itemMeta = item.getItemMeta();
			if(displayName != null) itemMeta.setDisplayName(displayName);
			if(lore != null) itemMeta.setLore(lore);
			item.setItemMeta(itemMeta);
		}
		return item;
	}
	public ItemStack createItem(Material material, Integer amount, Byte data, String displayName, List<String> lore)
	{
		ItemStack item = new ItemStack(material, amount, data);
		if(displayName != null || lore != null)
		{
			ItemMeta itemMeta = item.getItemMeta();
			if(displayName != null) itemMeta.setDisplayName(displayName);
			if(lore != null) itemMeta.setLore(lore);
			item.setItemMeta(itemMeta);
		}
		return item;
	}
	public ItemStack createItem(Material material, Integer amount, String displayName)
	{
		ItemStack item = new ItemStack(material, amount);
		if(displayName != null)
		{
			ItemMeta itemMeta = item.getItemMeta();
			if(displayName != null) itemMeta.setDisplayName(displayName);
			item.setItemMeta(itemMeta);
		}
		return item;
	}
	public ItemStack createItem(Material material, int amount, byte data, String displayName)
	{
		ItemStack item = new ItemStack(material, amount, data);
		if(displayName != null)
		{
			ItemMeta itemMeta = item.getItemMeta();
			if(displayName != null) itemMeta.setDisplayName(displayName);
			item.setItemMeta(itemMeta);
		}
		return item;
	}
	public ItemStack createItem(int material, Integer amount, Byte data, String displayName, List<String> lore)
	{
		@SuppressWarnings("deprecation")
		ItemStack item = new ItemStack(material, amount, data);
		if(displayName != null || lore != null)
		{
			ItemMeta itemMeta = item.getItemMeta();
			if(displayName != null) itemMeta.setDisplayName(displayName);
			if(lore != null) itemMeta.setLore(lore);
			item.setItemMeta(itemMeta);
		}
		return item;
	}
}