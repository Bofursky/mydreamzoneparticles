package mydreamzone.Particles.Managers;

import java.io.File;

import org.bukkit.configuration.file.YamlConfiguration;

import mydreamzone.Particles.Particle;

public class FileManager 
{
	public Particle plugin;
	public File configFile;
			
	public FileManager(Particle plugin)
	{
		this.plugin = plugin;
	}
	public YamlConfiguration loadMessages()
	{
		try 
        {
            if (!plugin.getDataFolder().exists())
            {
            	plugin.getDataFolder().mkdirs();
            }
            if (!new File(plugin.getDataFolder(), "Messages.yml").exists())
            {
            	plugin.saveResource("Messages.yml", false);
            }
            return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "Messages.yml"));           
        } 
		catch (Exception ex) 
        {
            plugin.getLogger().severe("Blad podczas wczytywania konfiguracji! (Messages.yml)");
            plugin.getServer().getPluginManager().disablePlugin(plugin);
            return null;
        }
	}
	public YamlConfiguration loadConfig()
	{
		try 
        {
            if (!plugin.getDataFolder().exists())
            {
            	plugin.getDataFolder().mkdirs();
            }
            if (!new File(plugin.getDataFolder(), "Config.yml").exists())
            {
            	plugin.saveResource("Config.yml", false);
            }
            return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "Config.yml"));           
        } 
		catch (Exception ex) 
        {
            plugin.getLogger().severe("Blad podczas wczytywania konfiguracji! (Config.yml)");
            plugin.getServer().getPluginManager().disablePlugin(plugin);
            return null;
        }
	}
}
