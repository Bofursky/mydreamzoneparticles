package mydreamzone.Particles;

import java.util.LinkedHashMap;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import mydreamzone.Particles.Commands.MenuCommand;
import mydreamzone.Particles.Commands.TestCommand;
import mydreamzone.Particles.Data.ConfigData;
import mydreamzone.Particles.Data.MessageData;
import mydreamzone.Particles.Externals.ParticleEffect;
import mydreamzone.Particles.Listeners.InventoryListener;
import mydreamzone.Particles.Listeners.ParticlesListener;
import mydreamzone.Particles.Listeners.PlayerListener;
import mydreamzone.Particles.Managers.FileManager;
import mydreamzone.Particles.Managers.ItemManager;
import mydreamzone.Particles.Managers.VaultManager;
import mydreamzone.Particles.Menu.ParticlesMenu;

public class Particle extends JavaPlugin
{
	public LinkedHashMap<String, ParticleEffect> playerEffect = new LinkedHashMap<String, ParticleEffect>();	
	public Particle plugin;
	public FileManager fileManager;
	public VaultManager vaultManager;
	public ItemManager itemManager;
	public ParticlesListener particlesListener;
	public MessageData messageData;
	public ConfigData configData;
	public ParticlesMenu mainMenu;
	public BukkitTask bukkitTask;		
	
    public void onEnable() 
    {
		enableSettings();  
        enablelisteners();
        enableCommands();  
        
        bukkitTask = getServer().getScheduler().runTask(this, new Timer(this));
    }
    public void onDisable()
	{ 
		//plugin.bukkitTask.cancel();
	}
	private void enablelisteners()
	{	
    	PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new ParticlesListener(this), this);
        pm.registerEvents(new PlayerListener(this), this);
        pm.registerEvents(new InventoryListener(this), this);
	}
	private void enableSettings()
	{	
		fileManager = new FileManager(this);
		vaultManager = new VaultManager();
		itemManager = new ItemManager();
		
		messageData = new MessageData(fileManager.loadMessages());	
		configData = new ConfigData(fileManager.loadConfig());	
		
		mainMenu = new ParticlesMenu(this);
	}
	private void enableCommands()
	{
		getCommand("Test").setExecutor( new TestCommand(this));
		getCommand("Particle").setExecutor( new MenuCommand(this));
	}
}
