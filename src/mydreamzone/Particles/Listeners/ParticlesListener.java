package mydreamzone.Particles.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import mydreamzone.Particles.Particle;
import mydreamzone.Particles.Externals.ParticleEffect;
public class ParticlesListener implements Listener
{
	public Particle plugin;
	private ParticleEffect eff;
	
	public ParticlesListener(Particle plugin)
	{
		this.plugin = plugin;
	}
	@EventHandler
	public void Click(InventoryClickEvent event)
	{
    	if (event.getInventory() == null)
    	{
    		return;		
    	}
	    	if (event.getInventory().getName().equalsIgnoreCase("�6Lista dostepnych efektow"))
	    	{
	    		try
	    		{
	    			
	    			Player player = (Player) event.getWhoClicked();
	    			String playerName = player.getName();	
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aHeart"))
		    	    {		    				
	    		        eff = ParticleEffect.HEART;
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();   				
		    	    }	
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aSpell"))
		    	    {	
	    				eff = ParticleEffect.SPELL_MOB;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }	
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aFire"))
		    	    {	
	    				eff = ParticleEffect.BLOCK_DUST;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aCloud"))
		    	    {	
	    				eff = ParticleEffect.CLOUD;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aExplode"))
		    	    {	
	    				eff = ParticleEffect.EXPLOSION_LARGE;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aFireworks"))
		    	    {	
	    				eff = ParticleEffect.FIREWORKS_SPARK;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aSplash"))
		    	    {	
	    				eff = ParticleEffect.WATER_SPLASH;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aWake"))
		    	    {	
	    				eff = ParticleEffect.WATER_WAKE;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aSuspended"))
		    	    {	
	    				eff = ParticleEffect.SUSPENDED_DEPTH;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aCrit"))
		    	    {	
	    				eff = ParticleEffect.CRIT;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aMagic"))
		    	    {	
	    				eff = ParticleEffect.CRIT_MAGIC;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aSmoke"))
		    	    {	
	    				eff = ParticleEffect.SMOKE_LARGE;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aWitch"))
		    	    {	
	    				eff = ParticleEffect.SPELL_WITCH;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aWater"))
		    	    {	
	    				eff = ParticleEffect.DRIP_WATER;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aLava"))
		    	    {	
	    				eff = ParticleEffect.DRIP_LAVA;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aAngry"))
		    	    {	
	    				eff = ParticleEffect.VILLAGER_ANGRY;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aHappy"))
		    	    {	
	    				eff = ParticleEffect.VILLAGER_HAPPY;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aNote"))
		    	    {	
	    				eff = ParticleEffect.NOTE;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aPortal"))
		    	    {	
	    				eff = ParticleEffect.PORTAL;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aEnchantment"))
		    	    {	
	    				eff = ParticleEffect.ENCHANTMENT_TABLE;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aFlame"))
		    	    {	
	    				eff = ParticleEffect.FLAME;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aLava2"))
		    	    {	
	    				eff = ParticleEffect.LAVA;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aRedstone"))
		    	    {	
	    				eff = ParticleEffect.REDSTONE;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aSnowball"))
		    	    {	
	    				eff = ParticleEffect.SNOWBALL;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aSnow"))
		    	    {	
	    				eff = ParticleEffect.SNOW_SHOVEL;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aSlime"))
		    	    {	
	    				eff = ParticleEffect.SLIME;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aBarrier"))
		    	    {	
	    				eff = ParticleEffect.BARRIER;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aCrack"))
		    	    {	
	    				eff = ParticleEffect.BLOCK_CRACK;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�aAppearance"))
		    	    {	
	    				eff = ParticleEffect.MOB_APPEARANCE;   				  			
	    				plugin.playerEffect.put(playerName, eff);
	    				player.closeInventory();
		    	    }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�6MyDreamZone Particles"))
		    	    {	
	    				player.closeInventory();
		    	    }    			
	    		}
	    		catch (Exception localException) {}
	    	}
	}
}    				


