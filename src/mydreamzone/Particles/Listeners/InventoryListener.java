package mydreamzone.Particles.Listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import mydreamzone.Particles.Particle;

public class InventoryListener implements Listener
{
	public Particle plugin;	
	public InventoryListener(Particle plugin)
	{
		this.plugin = plugin;
	}
	@EventHandler
	public void onJoin(PlayerJoinEvent event)
	{		
		if(plugin.configData.ParticleGiveOnJoin == true )
		{
			Player player = event.getPlayer(); 
		 	PlayerInventory inv = player.getInventory();
		 	
		 	int mat = plugin.configData.ParticleID;	
	    	int amount = plugin.configData.ParticleAmount;
	    	byte data = (byte) plugin.configData.ParticleDurability;
	    	String displayName = plugin.configData.ParticleName;	    	
	    	ArrayList<String> lore = new ArrayList<String>();    	
	    	if(lore != null)
			{
	    		lore.addAll(plugin.configData.ParticleLore);
			}	
	    	int loc = plugin.configData.ParticleLocation;

	 		inv.setItem(loc, plugin.itemManager.createItem(mat, amount, data, displayName, lore));
		}
	}
	
	@EventHandler
	public void playerClicked(PlayerInteractEvent event)
	{
		if (((event.getAction() == Action.RIGHT_CLICK_AIR) || 
				(event.getAction() == Action.RIGHT_CLICK_BLOCK)) && 
				(event.getItem() != null))	
		{
    		if(event.getPlayer().getItemInHand() != null)
    		{
    			if(event.getPlayer().getItemInHand().hasItemMeta())
    			{
    				if(event.getPlayer().getItemInHand().getItemMeta().hasDisplayName())
    				{
    					if(event.getPlayer().getItemInHand().getItemMeta().getDisplayName().contains(plugin.configData.ParticleName))
    					{
    						Player player = event.getPlayer(); 
    		    			String playerName = player.getName();	
    		    			if(plugin.playerEffect.containsKey(playerName)) 
    		    			{
    		    				plugin.playerEffect.remove(playerName);
    		    			}		
    						plugin.mainMenu.Menu(event.getPlayer());
    					}
    				}
    			}
    		}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onDrop(PlayerDropItemEvent event)
	{
		if ((!plugin.configData.AllowDropItems) && event.getItemDrop().getItemStack().getTypeId() == plugin.configData.ParticleID) 
		{
			Item item = event.getItemDrop();
			ItemStack s = item.getItemStack();
			if (s.getItemMeta().getDisplayName().contains(plugin.configData.ParticleName))
            {
               event.setCancelled(true);
            }
			
		}			
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onDeath(PlayerDeathEvent event)
	{
		if (!plugin.configData.DropItemsOnDeath)
		{
			List<ItemStack> toRemove = new ArrayList<ItemStack>();
			for (ItemStack is : event.getDrops())
			{
			    if (is.getTypeId() == plugin.configData.ParticleID)
			    {
			    	if(is.hasItemMeta())
			    	{
			    		if(is.getItemMeta().hasDisplayName())
			    		{
			    			if(is.getItemMeta().getDisplayName().contains(plugin.configData.ParticleName))
			    				toRemove.add(is);
			    		}
			    	}
			    }
		    }
			for (ItemStack is : toRemove) 
			{
				event.getDrops().remove(is);
			}
			toRemove.clear();
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onMove(InventoryClickEvent event)
	{
	    if (!plugin.configData.AllowMoveItems)
	    {	
	    	if (event.getInventory() == null)
	    	{
	    		return;
	    	}
	    	if (event.getCurrentItem() == null)
	    	{
	    		return;
	    	}
	    	if (event.getCurrentItem().getTypeId() == plugin.configData.ParticleID) 
	    	{
	    		if (event.getCurrentItem().hasItemMeta())
	    		{
	    			if (event.getCurrentItem().getItemMeta().hasDisplayName())
	    			{
		    			if (event.getCurrentItem().getItemMeta().getDisplayName().contains(plugin.configData.ParticleName))// || (event.getCursor().getItemMeta().getDisplayName().contains(plugin.configData.ParticleName))) 
		    			{    			
		    				event.setCancelled(true);
		    			}
	    			}
	    		}
	    	}
	    }
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent event)
	{
	    if ((!plugin.configData.AllowPlaceItems)) 
	    {
	    	if(event.getPlayer().getItemInHand() != null)
	    	{
	    		if(event.getPlayer().getItemInHand().hasItemMeta())
	    		{
	    			if(event.getPlayer().getItemInHand().getItemMeta().hasDisplayName())
	    			{
	    				if(event.getPlayer().getItemInHand().getItemMeta().getDisplayName().contains(plugin.configData.ParticleName))
	    					event.setCancelled(true);
	    			}
	    		}
	    	}
		}
	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent event)
	{
		if (plugin.configData.GetItemsOnRespawn)
		{	
			if(plugin.configData.ParticleGiveOnJoin == true )
			{
				Player player = event.getPlayer(); 
			 	PlayerInventory inv = player.getInventory();
			 	
			 	int mat = plugin.configData.ParticleID;	
		    	int amount = plugin.configData.ParticleAmount;
		    	byte data = (byte) plugin.configData.ParticleDurability;
		    	String displayName = plugin.configData.ParticleName;	    	
		    	ArrayList<String> lore = new ArrayList<String>();    	
		    	if(lore != null)
				{
		    		lore.addAll(plugin.configData.ParticleLore);
				}	
		    	int loc = plugin.configData.ParticleLocation;

		 		inv.setItem(loc, plugin.itemManager.createItem(mat, amount, data, displayName, lore));
			}
		}
	}
}