package mydreamzone.Particles.Listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import mydreamzone.Particles.Particle;


public class PlayerListener implements Listener
{
	private Particle plugin;
	public PlayerListener(Particle plugin)
	{
		this.plugin = plugin;
	}
	@EventHandler
	public void onMove(InventoryClickEvent event)
	{
    	if (event.getInventory() == null)
    	{
    		return;		
    	}
    	if (event.getInventory().getName().equalsIgnoreCase("�6Lista dostepnych efektow"))
    	{
    		event.setCancelled(true);
    	}   	
	}
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		String playerName = event.getPlayer().getName();
		if(plugin.playerEffect.containsKey(playerName))
		{
			plugin.playerEffect.remove(playerName);
		}
	}	
}
