package mydreamzone.Particles.Commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mydreamzone.Particles.Particle;
import mydreamzone.Particles.Externals.ParticleEffectExtra;
import mydreamzone.Particles.Externals.ParticleEffectExtra.ParticleType;

public class TestCommand implements CommandExecutor
{
	public Particle plugin;
	public TestCommand(Particle plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{	
		
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}	
		else
		{
			Player player = (Player) sender;
			Location location = player.getLocation();
			 
			int radius = 0;
			 
			for(double arg = 0; arg <= 50; arg+=0.3)
			{
			double x = Math.cos(arg);
			double z = Math.sin(arg);
			double y = 0.2 * arg;
			
			Location loc = new Location(player.getWorld(), (float) (location.getX() + x), (float) (location.getY() + y), (float) (location.getZ() + z));
			 
			ParticleEffectExtra particle = new ParticleEffectExtra(ParticleType.HEART, 0, 1, radius);
			particle.sendToLocation(loc);
			}		
		}
		return true;
	}
}
//x East West  1..-1
//z South North 1..-1
//y High  0...n