package mydreamzone.Particles.Commands;

import mydreamzone.Particles.Particle;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MenuCommand implements CommandExecutor
{
	private Particle plugin;
	public MenuCommand(Particle plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{	
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}		
    	if(sender.hasPermission("MyDreamZoneParticles.Menu")) 
    	{	
			if (args.length >= 0) 				
			{	
				
				Player player = (Player) sender;
    			String playerName = player.getName();	
    			if(plugin.playerEffect.containsKey(playerName)) 
    			{
    				plugin.playerEffect.remove(playerName);
    			}			
    			plugin.mainMenu.Menu(player);		
				return true;			
			}
    	}
		return true;
	}
}