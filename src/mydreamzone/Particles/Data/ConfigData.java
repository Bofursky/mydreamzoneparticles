package mydreamzone.Particles.Data;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigData
{
	public boolean AllowPlaceItems;
	public boolean AllowMoveItems;
	public boolean AllowDropItems;
	public boolean DropItemsOnDeath;
	public boolean GetItemsOnRespawn;
	public String ParticleName;
	public int ParticleID;
	public int ParticleDurability;
	public int ParticleAmount;
	public List<String> ParticleLore;
	public boolean ParticleGiveOnJoin;
	public int ParticleLocation;

	public ConfigData(YamlConfiguration config)
	{
		AllowPlaceItems = config.getBoolean("Items.AllowPlaceItems", false);
		AllowMoveItems = config.getBoolean("Items.AllowMoveItems", true);
		AllowDropItems = config.getBoolean("Items.AllowDropItems", false);
		DropItemsOnDeath = config.getBoolean("Items.DropItemsOnDeath", false);
		GetItemsOnRespawn = config.getBoolean("Items.GetItemsOnRespawn", true);
		
		
		ParticleName = ChatColor.translateAlternateColorCodes('&',config.getString("Particle.Name", "Efekty Specjalne"));
		ParticleID = config.getInt("Particle.ID", 369);
		ParticleDurability = config.getInt("Particle.Durability", 0);
		ParticleAmount = config.getInt("ParticleAmount", 1);
		ParticleLore = config.getStringList("Particle.Lore");	
		ParticleGiveOnJoin = config.getBoolean("Particle.GiveOnJoin", false);
		ParticleLocation = config.getInt("Particle.Location", 8);
	}
}
