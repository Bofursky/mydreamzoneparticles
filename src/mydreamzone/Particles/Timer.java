package mydreamzone.Particles;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import mydreamzone.Particles.Particle;
import mydreamzone.Particles.Externals.ParticleEffectExtra;
import mydreamzone.Particles.Externals.ParticleEffect.BlockData;
import mydreamzone.Particles.Externals.ParticleEffectExtra.ParticleType;

public class Timer implements Runnable
{
	private Particle plugin;
	public BukkitTask bukkitTask;	
	public Timer(Particle plugin)
	{
		this.plugin = plugin;
	}	
	public void run() 
	{		
		bukkitTask = Bukkit.getScheduler().runTaskTimer(this.plugin, new Runnable() 
		{
			@Override
			public void run() 
			{				
				for(Player player : plugin.getServer().getOnlinePlayers())
				{
					String playerName = player.getName();
					if(plugin.playerEffect.containsKey(playerName))
					{
						Location loc = player.getLocation();
						String effectName = plugin.playerEffect.get(playerName).getName();
						if(effectName.equalsIgnoreCase("heart"))
						{
							for(int i = 0; i < 10; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 1.0F, 1, loc, 20);
							}
						}
						if(effectName.equalsIgnoreCase("mobSpell"))
						{
							for(int i = 0; i < 25; i++)
							{
			                    float x = (float) (Math.random()*1.5);
			                    float y = -0.5F;
			                    float z = (float) (Math.random()*1.5);
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}
						}
						if(effectName.equalsIgnoreCase("blockdust"))
						{
							for(int i = 0; i < 100; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								BlockData bc = new BlockData (Material.FIRE, (byte)  0);
								plugin.playerEffect.get(playerName).display(bc, x, y, z, 2.0F, 1, loc, 20);
							}	
						}
						if(effectName.equalsIgnoreCase("cloud"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}			
						}
						if(effectName.equalsIgnoreCase("largeexplode"))
						{
							plugin.playerEffect.get(playerName).display(0, 1, 0, 1F, 1, loc, 20);		    							    
						}
						if(effectName.equalsIgnoreCase("fireworksSpark"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("splash"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 1.0F, 3, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("wake"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 1.0F, 3, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("depthSuspend"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 1.0F, 3, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("crit"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 1.0F, 3, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("magicCrit"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 1.0F, 3, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("largesmoke"))
						{
							plugin.playerEffect.get(playerName).display(1, 1, 1, 1F, 15, loc, 20);	   							    
						}
						if(effectName.equalsIgnoreCase("witchMagic"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("fireworksSpark"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("dripWater"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("dripLava"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("angryVillager"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("happyVillager"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("townaura"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("note"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("portal"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 5, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("enchantmenttable"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 3, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("flame"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("lava"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("reddust"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("snowballpoof"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("snowshovel"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("slime"))
						{
							for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("barrier"))
						{
							for(int i = 0; i < 10; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("blockcrack"))
						{
							for(int i = 0; i < 50; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								BlockData bc = new BlockData (Material.CACTUS, (byte)  0);
								plugin.playerEffect.get(playerName).display(bc, x, y, z, 2.0F, 1, loc, 20);
							}	   							    
						}
						if(effectName.equalsIgnoreCase("mobappearance"))
						{
													 
						plugin.playerEffect.get(playerName).display(1, 1, 1, 0.1F, 1, loc, 1);
									
							
							/*for(int i = 0; i < 20; i++)
							{
								float x = (float)Math.random();
								float y = (float)Math.random();
								float z = (float)Math.random();
								plugin.playerEffect.get(playerName).display(x, y, z, 0.6F, 1, loc, 20);
							}
							*/	   							    
						}
					}
				}
				
			}		
		}, 10, 10);
	}
}